#-*- coding: utf8 -*-
from distutils.core import setup

setup(
    name='django-buglog',
    version='0.1.3',
    author=u'Adrian Lin',
    author_email='nildamului@gmail.com',
    packages=['buglog'],
    package_data={
        'buglog': ['templates/buglog/*'],
    },
    url='http://bitbucket.org/nildamului/buglog',
    license='BSD licence, see LICENCE',
    description='Help log the django bug pages',
    long_description=open('README.rst').read(),
    zip_safe=False,
)