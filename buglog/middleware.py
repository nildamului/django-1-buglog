#!-*- coding:utf8 -*-
import sys
from django.views.debug import ExceptionReporter, technical_500_response
from django.http import HttpResponseServerError
from settings import DEBUG
from buglog.process import exception_process


class ExceptionMiddleware(object):
    def process_exception(self, request, exception):
        record = exception_process(request, *sys.exc_info())
        if DEBUG: return HttpResponseServerError(record)
        else: return HttpResponseServerError(u"Server Error: %s"%record.identify)