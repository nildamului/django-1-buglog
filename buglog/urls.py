#!-*- coding:utf8 -*-
from django.conf.urls import patterns, include, url


urlpatterns = patterns('buglog.views',
    url(r"^$", 'rLogin'),
    url(r"^list/$", 'rBugList'),
    url(r"^bugpage/(?P<code>[0-9A-Z]+)/$", 'rBugPage'),
)