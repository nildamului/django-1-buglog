#!-*- coding:utf8 -*-
import sys
from os.path import join
from django.contrib import auth
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext as _
from django.template.loader import get_template
from django.template import RequestContext
from django.http import HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from buglog.models import Option, Exception, BugPage

def checkCanViewPage(view_function):
    def innerFunction(*args, **kw):
        user =  args[0].user
        url = reverse("buglog.views.rLogin")
        if not user.is_authenticated(): return HttpResponseRedirect(url)
        if not (user.is_staff or user.is_superuser): return HttpResponseRedirect(url)
        return view_function(*args, **kw)
    return innerFunction


def rLogin(R):
    user, data = R.user, R.POST
    msg = ""
    url = reverse("buglog.views.rBugList")
    if user.is_authenticated() and user.is_active and (user.is_staff or user.is_superuser): return HttpResponseRedirect(url)
    if data.has_key("submit"):
        user = auth.authenticate(username=data["username"], password=data["password"])
        if not user: msg = _("Wrong username or password!")
        elif not user.is_active: msg = _("This account has been closed!")
        elif not (user.is_staff or user.is_superuser): msg = _("Insufficient permissions!")
        else:
            auth.login(R, user)
            url = reverse(url)
            return HttpResponseRedirect(url)

    template = get_template(join("buglog", "login.html"))
    html = template.render(RequestContext(R, {"msg": msg}))
    return HttpResponse(html)


@checkCanViewPage
def rBugList(R):
    data = R.POST
    if data.has_key("submit"):
        case = Exception.objects.get(id=data["exception_id"])
        case.delete()
        url = reverse("buglog.views.rBugList")
        return HttpResponseRedirect(url)

    exception = Exception.objects.all().order_by("-id")
    template = get_template(join('buglog', 'buglist.html'))
    html = template.render(RequestContext(R, {"exception": exception}))
    return HttpResponse(html)


@checkCanViewPage
def rBugPage(R, code):
    bug = BugPage.objects.get(code=code)
    return HttpResponse(bug.html)