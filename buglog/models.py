#!-*- coding:utf8 -*-
from django.db import models as M
from django.utils.translation import ugettext as _


class Option(M.Model):
    swarm = M.CharField(verbose_name=_("Option Swarm"), max_length=64)
    value = M.CharField(verbose_name=_("Option Value"), max_length=64)
    
    def __unicode__(self):
        return "%s: %s" % (self.swarm, self.value)



class Exception(M.Model):
    code = M.CharField(verbose_name=_("Exception Code"), max_length=8, null=True)
    type = M.ForeignKey(Option, verbose_name=_("Exception Type"), related_name="exception_type")
    value = M.TextField(verbose_name=_("Exception Value"))
    module = M.CharField(verbose_name=_("Exception Module"), max_length=255)
    function = M.CharField(verbose_name=_("Exception Function"), max_length=255)
    url = M.TextField(verbose_name=_("Request URL"))
    ajax = M.BooleanField(verbose_name=_("Is AJAX Request"), default=False)
    solved = M.BooleanField(verbose_name=_("Solved Status"), default=False)
    
    def __unicode__(self):
        return self.code
    
    def rBugNumber(self):
        bugs = self.bug_page.all()
        return len(bugs)
    
    def rLastTime(self):
        return self.bug_page.all().order_by("-time")[0].time
    
    def rBugs(self):
        return self.bug_page.all().order_by("-time")



class BugPage(M.Model):
    exception = M.ForeignKey(Exception, related_name=_("bug_page"))
    code = M.CharField(verbose_name=_("Page Code"), max_length=4, null=True)
    time = M.DateTimeField(verbose_name=_("Recorded Time"), auto_now_add=True)
    html = M.TextField(verbose_name=_("Page Content"))
    solved = M.BooleanField(verbose_name=_("Solved Status"), default=False)
    
    def __unicode__(self):
        return self.code