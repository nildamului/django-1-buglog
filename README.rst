=============
django-buglog
=============

Help log the django bug pages.

Currently in beta (v0.1.0).


Requirements
============

Core
----

* Python 2.6+
* Django 1.5+


Usage
=====
* Add 'buglog.middleware.ExceptionMiddleware' into MIDDLEWARE_CLASSES in settings.py
* Set url(r'^budget$', 'budget', name='budget') in urls.py